import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Geolocation , Geoposition } from '@ionic-native/geolocation/ngx';
import { PermissionService } from '../services/permission.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private permission: PermissionService,
    private geoloc: Geolocation,
    private changeDetector: ChangeDetectorRef
  ) {}

  location: any;

  async ngOnInit() {
     if(await this.permission.askPermissionGps()) {
       this.geoloc.watchPosition({
        enableHighAccuracy: true,
        timeout: 500,
        maximumAge: 0
       }).subscribe(data => {
          if((<PositionError>data).code != null) {
           console.log(data);
           return;
          }
          this.location = (<Geoposition>data).coords;
          this.changeDetector.detectChanges();
          console.log(this.location.latitude, this.location.longitude);
       }, console.log)
     }
     else{
       console.log(42);
     }
  }



}
